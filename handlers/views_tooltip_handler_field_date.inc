<?php

class views_tooltip_handler_field_date extends views_handler_field_date {
  function option_definition() {
    $options = parent::option_definition();

    $options['tooltip_text'] = array('default' => '', 'translatable' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['tooltip_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Tooltip text'),
      '#description' => t('The text to display for this field. You may include HTML. You may enter data from this view as per the "Replacement patterns" below.'),
      '#default_value' => $this->options['tooltip_text'],
      '#fieldset' => 'style_settings',
    );
  }

  function render_text($alter) {
    $tokens = $this->get_render_tokens($alter);
    $theme_variables = array(
      'tooltip' => str_replace(array_keys($tokens), array_values($tokens), $this->options['tooltip_text']),
      'content' => parent::render_text($alter),
    );
    return theme('views_tooltip', $theme_variables);
  }
}