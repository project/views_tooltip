<?php

/**
 * @file
 */

/**
 * Implements hook_views_data_alter().
 * Replaces the original views handlers with our own
 * (that can handle the tooltips).
 */
function views_tooltip_views_data_alter(&$data) {
  $handler_map = array(
    'views_handler_field' => 'views_tooltip_handler_field',
    'views_handler_field_field' => 'views_tooltip_handler_field_field',
    'views_handler_field_boolean' => 'views_tooltip_handler_field_boolean',
    'views_handler_field_date' => 'views_tooltip_handler_field_date',
    'views_handler_field_numeric' => 'views_tooltip_handler_field_numeric',
    'views_handler_field_string' => 'views_tooltip_handler_field_string',
  );

  foreach ($data as $base_table => $fields) {
    foreach ($fields as $field_name => $field) {
      if (isset($field['field']) && isset($field['field']['handler'])) {
        $original_handler = $field['field']['handler'];
        if (isset($handler_map[$original_handler])) {
          $data[$base_table][$field_name]['field']['handler'] = $handler_map[$original_handler];
        }
      }
    }
  }
}